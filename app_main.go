package main

import (
	"errors"
	"fmt"
	"html/template"
	"net/http"

	"bitbucket.org/flaviocfoliveira/sessions"
	"bitbucket.org/flaviocfoliveira/users"
	"bitbucket.org/flaviocfoliveira/users/UsersValidation"
	"github.com/gin-gonic/gin"
)

func main() {

	router := gin.Default()
	router.SetFuncMap(template.FuncMap{
		"errorsFor":     errorsFor,
		"errorClassFor": errorClassFor,
	})
	router.LoadHTMLGlob("templates/*")
	router.Static("/static", "./static")

	router.Use(Authenticate())
	router.Use(AuditContextInjector())

	router.GET("/account/confirm", confirmEmailGET)

	pub := router.Group("/").Use(RedirectAuthenticated())
	{
		pub.GET("/", index)
		pub.POST("/login", login)

		pub.GET("/forgot-password", forgotPasswordGet)
		pub.POST("/forgot-password", forgotPasswordPost)

		pub.GET("/reset-password", resetPasswordGet)
		pub.POST("/reset-password", resetPasswordPost)

		pub.GET("/register", registerGet)
		pub.POST("/register", registerPost)

	}

	account := router.Group("/account").Use(RequiresAuthentication())
	{
		account.GET("dashboard", dashboard)
		account.GET("logout", logout)

		account.GET("change-password", changePasswordGet)
		account.POST("change-password", changePasswordPost)

		account.GET("confirm-newtoken", confirmEmailGETNewToken)
	}

	admin := router.Group("/admin").Use(RequiresAuthentication())
	{
		admin.GET("admin", dashboard)
	}

	views := router.Group("/users").Use(RequiresAuthentication())
	{
		views.GET("", ViewUserList)
		views.GET("create", ViewUserCreate)
		views.GET("detail/:id", ViewUserDetails)
	}

	api := router.Group("/api").Use(RequiresAuthentication())
	{
		api.POST("users", apiCreateUser)
		api.POST("users/search", apiSearchUsers)
		api.GET("users/:id", apiGetUser)
		api.PUT("users/:id", apiUpdateUser)
		api.PUT("users/:id/active/:val", apiIsActiveUser)
		api.PUT("users/:id/locked/:val", apiIsLockedUser)
		api.PUT("users/:id/reset-password/:val", apiMustResetPasswordUser)
		api.PUT("users/:id/new-password/:val", apiNewPasswordUser)
	}

	err := router.Run(fmt.Sprintf("%s:%v", Config.Host.HostName, Config.Host.Port))
	if err != nil {
		panic(err)
	}

}

func index(c *gin.Context) {

	loginData := Login{
		Email:    "",
		Password: "",
		Csrf:     users.GenerateID(32),
	}

	c.HTML(http.StatusOK, "index.html", loginData)
}

func login(c *gin.Context) {

	var data Login
	_ = c.Bind(&data)

	if len(data.Email) == 0 {
		data.ErrorList = append(data.ErrorList, ErrorItem{
			Field:   "email",
			Message: "Campo obrigatório",
		})
	}
	if len(data.Password) == 0 {
		data.ErrorList = append(data.ErrorList, ErrorItem{
			Field:   "password",
			Message: "Campo obrigatório",
		})
	}

	if len(data.ErrorList) > 0 {
		c.HTML(http.StatusBadRequest, "index.html", data)
		c.Abort()
		return
	}

	var u *users.User
	manager := UsersSvc.CreateManager(CurrentAuditContext(c))
	accepted, err := manager.Authenticate(data.Email, data.Password, nil, func(user *users.User) {
		u = user
	})
	if !accepted || err != nil {
		switch err {
		case users.UserNotFound:
			data.ErrorList = append(data.ErrorList, ErrorItem{
				Message: "Dados de acesso inválidos",
			})
		case users.UserInactive:
			data.ErrorList = append(data.ErrorList, ErrorItem{
				Field:   "email",
				Message: "Dados de acesso inválidos",
			})
		case users.UserLocked:
			data.ErrorList = append(data.ErrorList, ErrorItem{
				Field:   "email",
				Message: "Utilizador bloqueado",
			})
		case users.UserSoftLocked:
			data.ErrorList = append(data.ErrorList, ErrorItem{
				Message: "Utilizador temporariamente bloqueado",
			})
		case users.UserMustResetPassword:
			data.ErrorList = append(data.ErrorList, ErrorItem{
				Message: "User must reset password",
			})
		case users.UserWrongPassword:
			data.ErrorList = append(data.ErrorList, ErrorItem{
				Message: "Dados de acesso inválidos",
			})
		}

		c.HTML(http.StatusBadRequest, "index.html", data)
		c.Abort()
		return
	}

	_, _ = StartSession(c, sessions.Session{
		PublicID: u.PublicID,
	})

	c.Redirect(302, Config.Auth.DefaultUrl)
}
func logout(c *gin.Context) {

	ctx := CurrentAuditContext(c)
	if ctx == nil {
		panic(errors.New("audit context not present"))
	}

	http.Redirect(c.Writer, c.Request, Config.Auth.LoginUrl, http.StatusFound)
	sessionID := ""
	userEmail := ""

	if u, exists := c.Get(CURRENT_USER); exists {
		userEmail = u.(*users.User).Email
	}

	if s, exists := c.Get(CURRENT_SESSION); exists {
		sessionID = s.(*sessions.Session).ID
		SessionManager.Terminate(sessionID)
	}

	RemoveSessionCookie(c)

	ctx.Audit(AuditModuleName, "logout", userEmail, "SUCCESS", fmt.Sprintf(`{"SessionID": "%s"}`, sessionID))

}

func forgotPasswordGet(c *gin.Context) {
	c.HTML(http.StatusOK, "ForgotPassword.html", nil)
}
func forgotPasswordPost(c *gin.Context) {

	data := ResetPassword{
		Email:     "",
		ErrorList: make([]ErrorItem, 0),
		SendEmail: false,
	}

	if email, exists := c.GetPostForm("txtEmail"); exists && len(email) > 0 {

		data.Email = email
		manager := UsersSvc.CreateManager(CurrentAuditContext(c))

		if u, err := manager.GetUserByEmail(email); err != nil {

			if err == users.UserNotFound {
				data.ErrorList = append(data.ErrorList, ErrorItem{
					Field:   "email",
					Message: "Account not found",
				})
			} else {
				data.ErrorList = append(data.ErrorList, ErrorItem{
					Message: err.Error(),
				})
			}

		} else {

			if token, err := manager.CreatePasswordRecoveryToken(u.PublicID); err != nil {

				data.ErrorList = append(data.ErrorList, ErrorItem{
					Message: err.Error(),
				})

			} else {

				url := fmt.Sprintf("%s%s", Config.Host.BaseURL, Config.Auth.ResetPasswordUrl)
				subject := fmt.Sprintf("Password reset on %s", Config.Host.WebsiteName)

				if err := MailsManager.SendForgotPasswordEmail(*u, token, url, subject); err != nil {

					data.ErrorList = append(data.ErrorList, ErrorItem{
						Message: err.Error(),
					})

				} else {

					data.SendEmail = true

				}

			}

		}

	} else {

		data.ErrorList = append(data.ErrorList, ErrorItem{
			Field:   "email",
			Message: ptLang.Get("Required", "Required field"),
		})

	}

	c.HTML(http.StatusOK, "ForgotPassword.html", data)
}

func resetPasswordGet(c *gin.Context) {

	data := ResetPasswordToken{
		Token:           "",
		NewPassword:     "",
		NewPasswordConf: "",
		ErrorList:       nil,
		IsSuccess:       false,
		Exists:          false,
	}

	if t, exists := c.GetQuery("t"); exists {

		data.Token = t
		manager := UsersSvc.CreateManager(CurrentAuditContext(c))
		if dbTokem, err := manager.GetPasswordRecoveryToken(data.Token, false); err != nil {

			if err == users.ResetPasswordTokenNotFound {
				data.Exists = false
			} else {
				data.ErrorList = append(data.ErrorList, ErrorItem{
					Message: err.Error(),
				})
			}

		} else {

			data.Exists = dbTokem != nil

		}

	}

	c.HTML(http.StatusOK, "ResetPassword.html", data)
}
func resetPasswordPost(c *gin.Context) {

	data := ResetPasswordToken{
		Token:           "",
		NewPassword:     "",
		NewPasswordConf: "",
		ErrorList:       nil,
		IsSuccess:       false,
		Exists:          false,
	}

	if t, exists := c.GetQuery("t"); exists {

		manager := UsersSvc.CreateManager(CurrentAuditContext(c))
		data.Token = t
		if dbTokem, err := manager.GetPasswordRecoveryToken(data.Token, false); err != nil {

			if err == users.ResetPasswordTokenNotFound {
				data.Exists = false
			} else {
				data.ErrorList = append(data.ErrorList, ErrorItem{
					Message: err.Error(),
				})
			}

		} else {

			data.Exists = dbTokem != nil
			pass, _ := c.GetPostForm("txtNewPassword")
			passConf, _ := c.GetPostForm("txtNewPasswordConf")

			err := manager.ResetPassword(dbTokem.PublicID, "", pass, passConf, false)
			if err != nil {

				data.ErrorList = make([]ErrorItem, 0)
				for _, e := range err.(UsersValidation.ErrorItems) {
					data.ErrorList = append(data.ErrorList, ErrorItem{
						Field:   e.Field,
						Message: e.Translate(ptLang),
					})
				}

			} else {

				if _, err := manager.DeletePasswordRecoveryToken(data.Token); err != nil {

					data.ErrorList = append(data.ErrorList, ErrorItem{
						Message: err.Error(),
					})

				} else {
					data.Token = ""
					data.IsSuccess = true
				}

			}

		}

	}

	c.HTML(http.StatusOK, "ResetPassword.html", data)
}

func registerGet(c *gin.Context) {

	data := RegisterRequest{
		Name:         "",
		Email:        "",
		Password:     "",
		PasswordConf: "",
		Csrf:         users.GenerateID(32),
	}

	c.HTML(http.StatusOK, "Register.html", data)
}
func registerPost(c *gin.Context) {

	var data RegisterRequest
	_ = c.Bind(&data)

	manager := UsersSvc.CreateManager(CurrentAuditContext(c))

	publicID, err := manager.CreateUser(data.Name, data.Email, data.Password, data.PasswordConf)
	if err != nil {
		data.ErrorList = make([]ErrorItem, 0)
		for _, e := range err.(UsersValidation.ErrorItems) {
			data.ErrorList = append(data.ErrorList, ErrorItem{
				Field:   e.Field,
				Message: e.Translate(ptLang),
			})
		}

		c.HTML(http.StatusBadRequest, "Register.html", data)
		c.Abort()
		return
	}

	token, err := manager.GenerateConfirmationToken(publicID, true)
	usr, _ := manager.GetUserByPublicID(publicID)

	err = MailsManager.SendAccountConfirmationEmail(*usr, token, fmt.Sprintf("%s%s", Config.Host.BaseURL, Config.Auth.ConfirmEmailUrl))
	if err != nil {
		panic(err)
	}

	c.HTML(http.StatusOK, "RegisterConfirmed.html", data)
}

func confirmEmailGET(c *gin.Context) {

	// Check first if a authenticated user is already confirmed
	u, exist := c.Get(CURRENT_USER)
	if exist {
		if u.(*users.User).IsConfirmed {
			// has nothing to do here
			c.Redirect(http.StatusFound, Config.Auth.DefaultUrl)
			c.Abort()
			return
		}
	}

	result := ConfirmEmail{
		IsConfirmed: false,
		ErrorList:   make([]ErrorItem, 0),
		NewTokenUrl: Config.Auth.ConfirmEmailRequestUrl,
	}
	if u != nil && u.(*users.User) != nil {
		result.User = u.(*users.User)
	}

	if t, exists := c.GetQuery("t"); exists {

		manager := UsersSvc.CreateManager(CurrentAuditContext(c))
		ok, err := manager.ConfirmToken(t)

		if !ok {

			switch err {
			case users.ConfirmationTokenNotFound:
				result.ErrorList = append(result.ErrorList, ErrorItem{
					Message: ptLang.Get(UsersValidation.InvalidConfirmationToken, "Invalid confirmation token"),
				})
			case users.UserNotFound:
				result.ErrorList = append(result.ErrorList, ErrorItem{
					Message: ptLang.Get(UsersValidation.InvalidConfirmationToken, "Invalid confirmation token"),
				})
			case users.UserInactive:
				result.ErrorList = append(result.ErrorList, ErrorItem{
					Message: ptLang.Get(UsersValidation.InvalidConfirmationToken, "Invalid confirmation token"),
				})
			case users.UserLocked:
				result.ErrorList = append(result.ErrorList, ErrorItem{
					Message: ptLang.Get(UsersValidation.InvalidConfirmationToken, "Invalid confirmation token"),
				})
			}

			c.HTML(http.StatusBadRequest, "ConfirmEmailError.html", result)
			return
		}

		c.HTML(http.StatusOK, "ConfirmEmailSuccess.html", nil)
		return
	}

	c.HTML(http.StatusOK, "ConfirmEmail.html", result)
}
func confirmEmailGETNewToken(c *gin.Context) {

	// Check first if a authenticated user is already confirmed
	u, exist := c.Get(CURRENT_USER)
	if exist {
		if u.(*users.User).IsConfirmed {
			// has nothing to do here
			c.Redirect(http.StatusFound, Config.Auth.DefaultUrl)
			c.Abort()
			return
		}
	}

	result := ConfirmEmail{
		IsConfirmed: false,
		ErrorList:   make([]ErrorItem, 0),
		User:        u.(*users.User),
	}

	manager := UsersSvc.CreateManager(CurrentAuditContext(c))
	usr := u.(*users.User)
	token, err := manager.GenerateConfirmationToken(usr.PublicID, true)

	err = MailsManager.SendAccountConfirmationEmail(*usr, token, fmt.Sprintf("%s%s", Config.Host.BaseURL, Config.Auth.ConfirmEmailUrl))
	if err != nil {
		panic(err)
	}

	c.HTML(http.StatusOK, "ConfirmEmail.html", result)
}

func dashboard(c *gin.Context) {

	data := DashboardData{
		BaseResponse: BaseResponse{
			User:      GetCurrentUser(c),
			Session:   GetCurrentSession(c),
			ErrorList: nil,
			Title:     "",
		},
	}

	RenderView(c, "", "dashboard", data)
}

func changePasswordGet(c *gin.Context) {

	cp := ChangePassword{}
	if u, exists := c.Get(CURRENT_USER); exists {
		cp.User = u.(*users.User)
	}
	if s, exists := c.Get(CURRENT_SESSION); exists {
		cp.Session = s.(*sessions.Session)
	}

	cp.OldPassword = ""
	cp.NewPassword = ""
	cp.NewPasswordConf = ""

	c.HTML(http.StatusOK, "ChangePassword.html", cp)
}
func changePasswordPost(c *gin.Context) {

	cp := ChangePassword{}
	if err := c.Bind(&cp); err != nil {
		cp.ErrorList = append(cp.ErrorList, ErrorItem{
			Message: err.Error(),
		})
		c.HTML(http.StatusBadRequest, "ChangePassword.html", cp)
		return
	}

	if u, exists := c.Get(CURRENT_USER); exists {
		cp.User = u.(*users.User)
	}
	if s, exists := c.Get(CURRENT_SESSION); exists {
		cp.Session = s.(*sessions.Session)
	}

	manager := UsersSvc.CreateManager(CurrentAuditContext(c))
	if err := manager.ResetPassword(cp.User.PublicID, cp.OldPassword, cp.NewPassword, cp.NewPasswordConf, true); err != nil {

		for _, e := range err.(UsersValidation.ErrorItems) {
			cp.ErrorList = append(cp.ErrorList, ErrorItem{
				Field:   e.Field,
				Message: e.Translate(ptLang),
			})
		}

		c.HTML(http.StatusBadRequest, "ChangePassword.html", cp)
		return
	}

	c.HTML(http.StatusOK, "ChangePasswordConfirmation.html", cp)
}

func RenderView(c *gin.Context, master string, page string, data interface{}) {

	tName := "index"
	if len(page) > 0 {
		tName = page
	}
	tmpl, exists := AppTemplates[fmt.Sprintf("%s.html", tName)]
	if !exists {
		c.Status(404)
		return
	}

	masterName := "master"
	if len(master) > 0 {
		masterName = master
	}
	err := tmpl.ExecuteTemplate(c.Writer, masterName, data)
	if err != nil {
		c.JSON(500, err)
		return
	}

	c.Status(200)

}
