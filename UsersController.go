package main

import (
	"bitbucket.org/flaviocfoliveira/users"
	"bitbucket.org/flaviocfoliveira/users/UsersValidation"
	"github.com/gin-gonic/gin"
)

type (
	UsersList struct {
		BaseResponse
		users.SearchResult
	}
	UserDetail struct {
		BaseResponse
		CurrentID string
	}
	UserCreate struct {
		BaseResponse

		fullName     string `json:"fullName"`
		email        string
		password     string
		passwordConf string
	}
	CreateUserRequest struct {
		FullName     string `json:"fullName"`
		Email        string `json:"email"`
		Password     string `json:"password"`
		PasswordConf string `json:"passwordConf"`
	}
)

func ViewUserList(c *gin.Context) {

	data := UsersList{
		BaseResponse: BaseResponse{
			User:      GetCurrentUser(c),
			Session:   GetCurrentSession(c),
			ErrorList: nil,
			Title:     "Users list",
		},
	}

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		sr := users.SearchRequest{}
		result := man.SearchUsers(sr)
		data.SearchResult = result

		RenderView(c, "", "users-list", data)
	}

}
func ViewUserDetails(c *gin.Context) {

	data := UserDetail{
		BaseResponse: BaseResponse{
			User:      GetCurrentUser(c),
			Session:   GetCurrentSession(c),
			ErrorList: nil,
			Title:     "",
		},
	}

	publicID := c.Param("id")

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		if !man.ExistPublicID(publicID) {

			c.Status(404)
			return

		} else {

			data.CurrentID = publicID
			RenderView(c, "", "user-detail", data)

		}

	}

}
func ViewUserCreate(c *gin.Context) {

	data := UserCreate{
		BaseResponse: BaseResponse{
			User:      GetCurrentUser(c),
			Session:   GetCurrentSession(c),
			ErrorList: nil,
			Title:     "",
		},
	}

	RenderView(c, "", "user-new", data)

}

func apiSearchUsers(c *gin.Context) {

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		var sr users.SearchRequest
		if err := c.BindJSON(&sr); err != nil {
			c.JSON(400, err)
			return
		}

		result := man.SearchUsers(sr)

		c.JSON(200, result)
		return
	}

	c.Status(404)
}

func apiGetUser(c *gin.Context) {

	id := c.Param("id")
	if len(id) == 0 {
		c.JSON(400, nil)
		return
	}

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		if u, err := man.GetUserByPublicID(id); err != nil {

			if err == users.UserNotFound {
				c.Status(404)
			} else {
				c.JSON(500, err)
			}

			return
		} else {

			c.JSON(200, u)
			return
		}

	}

	c.Status(404)
}
func apiUpdateUser(c *gin.Context) {

	id := c.Param("id")
	if len(id) == 0 {
		c.JSON(400, nil)
		return
	}

	type anon struct {
		FullName string
	}

	var postData anon
	if err := c.BindJSON(&postData); err != nil {
		c.JSON(400, err)
		return
	}

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		if err := man.UpdateUserName(id, postData.FullName); err != nil {

			if err == users.UserNotFound {
				c.Status(404)
			} else {
				c.JSON(500, err)
			}

			return
		} else {

			c.JSON(200, nil)
			return
		}

	}

	c.Status(404)
}

func apiIsActiveUser(c *gin.Context) {

	id := c.Param("id")
	val := c.Param("val")
	if len(id) == 0 || len(val) == 0 {
		c.JSON(400, nil)
		return
	}

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		var v = val == "1"
		if err := man.UpdateUserActive(id, v); err != nil {
			if err == users.UserNotFound {
				c.Status(404)
			} else {
				c.JSON(500, err)
			}
		} else {
			c.JSON(200, nil)
		}

	}

}
func apiIsLockedUser(c *gin.Context) {

	id := c.Param("id")
	val := c.Param("val")
	if len(id) == 0 || len(val) == 0 || (val != "0" && val != "1") {
		c.JSON(400, nil)
		return
	}

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		var err error
		if val == "0" {
			err = man.UnLockUser(id)
		} else if val == "1" {
			err = man.LockUser(id)
		}

		if err != nil {
			if err == users.UserNotFound {
				c.Status(404)
			} else {
				c.JSON(500, err)
			}
		} else {
			c.JSON(200, nil)
		}
	}
}

func apiMustResetPasswordUser(c *gin.Context) {

	id := c.Param("id")
	val := c.Param("val")
	if len(id) == 0 || len(val) == 0 || (val != "0" && val != "1") {
		c.JSON(400, nil)
		return
	}

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		b := val == "1"

		err := man.UpdateUserMustResetPassword(id, b)
		if err != nil {
			if err == users.UserNotFound {
				c.Status(404)
			} else {
				c.JSON(500, err)
			}
		} else {
			c.JSON(200, nil)
		}
	}

}
func apiNewPasswordUser(c *gin.Context) {

	id := c.Param("id")
	val := c.Param("val")

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		data := ApiResponse{}
		err := man.ResetPasswordForced(id, val)
		if err != nil {
			if err == users.UserNotFound {
				c.Status(404)
			} else {
				data.ErrorList = make([]ErrorItem, 0)
				for _, e := range err.(UsersValidation.ErrorItems) {
					data.ErrorList = append(data.ErrorList, ErrorItem{
						Field:   e.Field,
						Message: e.Translate(ptLang),
					})
				}
				data.Success = false
				c.JSON(400, data)
			}
		} else {
			c.JSON(200, nil)
		}
	}

}

func apiCreateUser(c *gin.Context) {

	var u CreateUserRequest
	c.BindJSON(&u)

	if man := UsersSvc.CreateManager(CurrentAuditContext(c)); man != nil {

		r := ApiResponse{}
		id, err := man.CreateUser(u.FullName, u.Email, u.Password, u.PasswordConf)
		if err != nil {

			r.ErrorList = make([]ErrorItem, 0)
			for _, e := range err.(UsersValidation.ErrorItems) {
				r.ErrorList = append(r.ErrorList, ErrorItem{
					Field:   e.Field,
					Message: e.Translate(ptLang),
				})
			}
			r.Success = false
			c.JSON(400, r)

		} else {

			r.Success = true
			r.Data = id

			c.JSON(201, r)
		}
	}

}
