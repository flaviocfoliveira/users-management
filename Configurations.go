package main

import (
	"bitbucket.org/flaviocfoliveira/audit"
	"bitbucket.org/flaviocfoliveira/sessions"
	"bitbucket.org/flaviocfoliveira/users"

	"bitbucket.org/flaviocfoliveira/users-management/emails"
)

type Configurations struct {
	ConnectionString string               `json:"ConnectionString"`
	Host             *HostSettings        `json:"Host"`
	UsersSettings    *users.Settings      `json:"Users"`
	Sessions         *sessions.Settings   `json:"Sessions"`
	Auth             *AuthSettings        `json:"Auth"`
	Smtp             *emails.SmtpSettings `json:"Smtp"`
	Audit            *audit.AuditSettings `json:"Audit"`
}
type HostSettings struct {
	HostName    string `json:"HostName"`
	Port        int    `json:"Port"`
	BaseURL     string `json:"BaseURL"`
	WebsiteName string `json:"WebsiteName"`
}
