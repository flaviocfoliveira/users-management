package main

import (
	"net/http"
	"time"

	"bitbucket.org/flaviocfoliveira/sessions"
	"bitbucket.org/flaviocfoliveira/users"
	"github.com/gin-gonic/gin"
)

const (
	CURRENT_USER    = "CurrentUser"
	CURRENT_SESSION = "CurrentSession"
)

type AuthSettings struct {
	SignUpUrl              string `json:"SignUpUrl"`
	LoginUrl               string `json:"LoginUrl"`
	LogoutUrl              string `json:"LogoutUrl"`
	DefaultUrl             string `json:"DefaultUrl"`
	ChangePasswordUrl      string `json:"ChangePasswordUrl"`
	CookieName             string `json:"CookieName"`
	CookieDomain           string `json:"CookieDomain"`
	CookiePath             string `json:"CookiePath"`
	ConfirmEmailUrl        string `json:"ConfirmEmailUrl"`
	ConfirmEmailRequestUrl string `json:"ConfirmEmailRequestUrl"`
	ResetPasswordUrl       string `json:"ResetPasswordUrl"`
}

func Authenticate() gin.HandlerFunc {
	return func(c *gin.Context) {

		v, _ := c.Cookie(Config.Auth.CookieName)
		if len(v) > 0 {

			// GET SESSION
			s, _ := SessionManager.GetSession(v, true)
			if s != nil && len(s.PublicID) > 0 && s.Expires.After(time.Now()) {

				manager := UsersSvc.CreateManager(nil)

				// GET USER
				u, _ := manager.GetUserByPublicID(s.PublicID)
				if u == nil {
					RemoveSessionCookie(c)
					c.Redirect(http.StatusFound, Config.Auth.LoginUrl)
					c.Abort()
					return
				}

				c.Set(CURRENT_USER, u)
				c.Set(CURRENT_SESSION, s)

			} else {

				RemoveSessionCookie(c)
				c.Redirect(http.StatusFound, Config.Auth.LoginUrl)
				c.Abort()
				return
			}
		}

		c.Next()
	}
}

func RequiresAuthentication() gin.HandlerFunc {
	return func(c *gin.Context) {

		path := c.FullPath()

		u, uExist := c.Get(CURRENT_USER)
		if !uExist {
			c.Redirect(http.StatusFound, Config.Auth.LoginUrl)
			c.Abort()
			return
		}

		usr := u.(*users.User)

		if usr.MustResetPassword && path != Config.Auth.ChangePasswordUrl {
			c.Redirect(http.StatusFound, Config.Auth.ChangePasswordUrl)
			c.Abort()
			return
		}

		if !usr.IsConfirmed && (path != Config.Auth.ConfirmEmailUrl && path != Config.Auth.ConfirmEmailRequestUrl) {
			c.Redirect(http.StatusFound, Config.Auth.ConfirmEmailUrl)
			c.Abort()
			return
		}

		c.Next()
	}
}
func RedirectAuthenticated() gin.HandlerFunc {
	return func(c *gin.Context) {

		if _, exist := c.Get(CURRENT_USER); exist {

			c.Redirect(http.StatusFound, Config.Auth.DefaultUrl)
			c.Abort()

			return
		}

		c.Next()
	}
}

func RemoveSessionCookie(c *gin.Context) {

	dt := time.Now().AddDate(0, 0, -1)
	cookie := http.Cookie{
		Name:     Config.Auth.CookieName,
		Value:    "",
		Path:     "/",
		Domain:   Config.Auth.CookieDomain,
		Expires:  dt,
		MaxAge:   -1,
		Secure:   false,
		HttpOnly: false,
	}
	http.SetCookie(c.Writer, &cookie)

}
func StartSession(c *gin.Context, s sessions.Session) (string, error) {

	oldSessionID, _ := c.Cookie(Config.Auth.CookieName)
	err := SessionManager.Terminate(oldSessionID)
	if err != nil {
		return "", err
	}

	newSessionID, err := SessionManager.CreateSession(s)
	if err != nil {
		return "", err
	}

	cookie := http.Cookie{
		Name:     Config.Auth.CookieName,
		Value:    newSessionID,
		Path:     Config.Auth.CookiePath,
		Domain:   Config.Auth.CookieDomain,
		MaxAge:   Config.Sessions.TTLMinutes * 60,
		Secure:   false,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(c.Writer, &cookie)

	return newSessionID, nil
}

func GetCurrentUser(c *gin.Context) *users.User {

	if u, exist := c.Get(CURRENT_USER); exist && u != nil {
		return u.(*users.User)
	}

	return nil
}
func GetCurrentSession(c *gin.Context) *sessions.Session {

	if s, exist := c.Get(CURRENT_SESSION); exist && s != nil {
		return s.(*sessions.Session)
	}

	return nil
}
