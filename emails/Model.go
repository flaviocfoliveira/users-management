package emails

type SmtpSettings struct {
	Server    string `json:"Server"`
	Port      int    `json:"Port"`
	UserName  string `json:"UserName"`
	Password  string `json:"Password"`
	EmailFrom string `json:"EmailFrom"`
	EmailTo   string `json:"EmailTo"`
}
