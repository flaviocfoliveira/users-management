package emails

import (
	"bytes"
	"fmt"
	"html/template"
	"log"

	"bitbucket.org/flaviocfoliveira/users"
	"gopkg.in/gomail.v2"
)

type (
	MailsManager struct {
		settings  SmtpSettings
		templates *template.Template
	}
	TokenConfirmation struct {
		FullName string
		Token    string
		Url      string
	}
)

func NewMailManager(settings SmtpSettings, templates *template.Template) MailsManager {
	return MailsManager{
		settings:  settings,
		templates: templates,
	}
}

func (m MailsManager) SendAccountConfirmationEmail(user users.User, token string, url string) error {

	messageData := TokenConfirmation{
		FullName: user.FullName,
		Token:    token,
		Url:      fmt.Sprintf(`%s?t=%s`, url, token),
	}

	var bfr bytes.Buffer
	err := m.templates.ExecuteTemplate(&bfr, "ConfirmAccount.html", messageData)
	if err != nil {
		return err
	}
	sBody := bfr.String()

	msg := gomail.NewMessage()
	msg.SetHeader("From", m.settings.EmailFrom)
	msg.SetHeader("To", user.Email)
	msg.SetHeader("Subject", "Account Confirmation ")
	msg.SetBody("text/html", sBody)

	d := gomail.NewDialer(m.settings.Server, m.settings.Port, m.settings.UserName, m.settings.Password)

	if err := d.DialAndSend(msg); err != nil {
		log.Print(err)
	}

	return nil
}

func (m MailsManager) SendForgotPasswordEmail(user users.User, token string, url string, subject string) error {

	messageData := TokenConfirmation{
		FullName: user.FullName,
		Token:    token,
		Url:      fmt.Sprintf(`%s?t=%s`, url, token),
	}

	var bfr bytes.Buffer
	err := m.templates.ExecuteTemplate(&bfr, "ForgotPassword.html", messageData)
	if err != nil {
		return err
	}
	sBody := bfr.String()

	msg := gomail.NewMessage()
	msg.SetHeader("From", m.settings.EmailFrom)
	msg.SetHeader("To", user.Email)
	msg.SetHeader("Subject", subject)
	msg.SetBody("text/html", sBody)

	d := gomail.NewDialer(m.settings.Server, m.settings.Port, m.settings.UserName, m.settings.Password)

	if err := d.DialAndSend(msg); err != nil {
		log.Print(err)
	}

	return nil
}
