package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"

	"bitbucket.org/flaviocfoliveira/audit"
	"bitbucket.org/flaviocfoliveira/sessions"
	"bitbucket.org/flaviocfoliveira/users"
	v "bitbucket.org/flaviocfoliveira/users/UsersValidation"

	"bitbucket.org/flaviocfoliveira/users-management/emails"
)

const (
	AuditModuleName = "App"
)

var Config *Configurations
var UsersSvc *users.Service
var SessionManager *sessions.MongodbStore
var ptLang v.Language
var AuditSvc *audit.AuditService

var MailsManager *emails.MailsManager

var AppTemplates map[string]*template.Template

func init() {

	var configFile string
	flag.StringVar(&configFile, "config", "config.json", "the path do the config.json file")
	flag.Parse()

	if b, err := ioutil.ReadFile(configFile); err != nil {
		panic(err)
	} else {

		var cfg Configurations
		if err := json.Unmarshal(b, &cfg); err != nil {
			panic(err)
		} else {

			Config = &cfg

			// Lang Messages
			if buffer, err := ioutil.ReadFile("UserMessages.json"); err != nil {
				panic(err)
			} else {
				if err := json.Unmarshal(buffer, &ptLang); err != nil {
					panic(err)
				}
			}

			// Users Service
			if svc, err := users.NewUsersService(Config.ConnectionString, Config.UsersSettings); err != nil {
				panic(err)
			} else {
				UsersSvc = svc
			}

			// Sessions
			if sess, err := sessions.NewMongodbStore(*Config.Sessions); err != nil {
				panic(err)
			} else {
				SessionManager = sess
			}

			mm := emails.NewMailManager(*Config.Smtp, template.Must(template.New("").ParseGlob("templates-email/*.html")))
			MailsManager = &mm

			if svc, err := audit.NewAuditService(*Config.Audit); err != nil {
				panic(err)
			} else {
				AuditSvc = svc
			}

			LoadTemplates()
		}
	}
}

func LoadTemplates() {

	baseTemplates := template.Must(template.New("Layout").ParseGlob("BaseTemplates/*.html"))

	f, err := ioutil.ReadDir("./pages")
	if err != nil {
		panic(err)
	}

	if len(f) > 0 {
		t := make(map[string]*template.Template)

		for _, file := range f {

			content, err := ioutil.ReadFile("pages/" + file.Name())
			if err != nil {
				panic(fmt.Sprintf("Failed to load page [%s]. %s", file.Name(), err))
			}

			tmpl := template.Must(baseTemplates.Clone())
			_, err = tmpl.Parse(string(content))
			if err != nil {
				panic(fmt.Sprintf("Failed to parse template [%s]. %s", file.Name(), err))
			}

			t[file.Name()] = tmpl
		}

		AppTemplates = t
	}

}
