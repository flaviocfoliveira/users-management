let app = new Vue({
    el: "#app",
    data: {

        Email: null,
        FullName: null,
        IsActive: null,
        IsConfirmed: null,
        IsLocked: null,
        MustResetPassword: null,
        OrderBy: null,
        OrderByDirection: null,
        PublicID: null,

        Items: [],
        Total: 0,

        Page: 1,
        PageSize: 25,
        TotalPages: 0,
        PageSizes: [25, 50, 100, 200],
    },
    methods: {
        ApplyFilter: function (searchFilter) {

            if (searchFilter !== undefined && searchFilter != null) {
                this.PublicID = searchFilter.PublicID;
                this.Email = searchFilter.Email;
                this.FullName = searchFilter.FullName;
                this.IsActive = GetBool(searchFilter.IsActive);
                this.IsConfirmed = GetBool(searchFilter.IsConfirmed);
                this.IsLocked = GetBool(searchFilter.IsLocked);
                this.MustResetPassword = GetBool(searchFilter.MustResetPassword);
            }

            this.Page = 1;

            this.search();
        },
        search: function () {

            let params = {
                PublicID: this.PublicID,
                Email: this.Email,
                FullName: this.FullName,
                IsActive: this.IsActive,
                IsConfirmed: this.IsConfirmed,
                IsLocked: this.IsLocked,
                MustResetPassword: this.MustResetPassword,
                OrderBy: this.OrderBy,
                OrderByDirection: this.OrderByDirection,
                Page: this.Page,
                PageSize: this.PageSize,
            };

            fetch("/api/users/search", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(params),
            })
                .then(response => response.json())
                .then((data) => {

                    console.log(data);

                    this.Items = data.Items;
                    this.Total = data.Total;

                })
                .catch((error) => {
                    console.log('Error:', error);
                });
        },
        OnPageChange: function (e) {
            this.Page = e.Page;
            this.PageSize = e.PageSize;
            this.search();
        },
        OnPagerUpdate: function (e) {
            this.Page = e.Page;
            this.TotalPages = e.TotalPages;
        },
        OnPageSizeUpdate: function () {
            this.Page = 1;
            this.search();
        },
    },
    delimiters: ['[[', ']]'],
    mounted() {
        this.search();
    },
});


Vue.component("search-users", {
    props: {
        Email: {default: ""},
        FullName: {default: ""},
        IsActive: {default: ""},
        IsConfirmed: {default: ""},
        IsLocked: {default: ""},
        MustResetPassword: {default: ""},
        PublicID: {default: ""},
    },
    methods: {
        doSearch: function () {
            this.$emit("search", {
                Email: this.Email,
                FullName: this.FullName,
                IsActive: this.IsActive,
                IsConfirmed: this.IsConfirmed,
                IsLocked: this.IsLocked,
                MustResetPassword: this.MustResetPassword,
                PublicID: this.PublicID,
            });
        },
        reset: function () {
            this.Email = "";
            this.FullName = "";
            this.IsActive = "";
            this.IsConfirmed = "";
            this.IsLocked = "";
            this.MustResetPassword = "";
            this.PublicID = "";
            this.doSearch();
        },
    },
    template: `<div class="side-block">
        <div class="title">
            <div class="text">Search</div>
            <div class="icons"><i class="fas fa-undo" v-on:click="reset()"></i></div>
        </div>
        <div class="content pr-10">
            <div class="form-field"><label for="txtPublicID">PublicID</label><input id="txtPublicID" type="text" v-model="PublicID" class="textbox"></div>
            <div class="form-field"><label for="txtFullName">FullName</label><input id="txtFullName" type="text" v-model="FullName" class="textbox"></div>
            <div class="form-field"><label for="txtEmail">Email</label><input id="txtEmail" type="text" v-model="Email" class="textbox"></div>
            <div class="form-field"><label for="ddlIsActive">IsActive</label><select-bool id="ddlIsActive" v-model="IsActive" class="dropdown"></select-bool></div>
            <div class="form-field"><label for="ddlIsConfirmed">IsConfirmed</label><select-bool id="ddlIsConfirmed" v-model="IsConfirmed" class="dropdown"></select-bool></div>
            <div class="form-field"><label for="ddlIsLocked">IsLocked</label><select-bool id="ddlIsLocked" v-model="IsLocked" class="dropdown"></select-bool></div>
            <div class="form-field"><label for="ddlMustResetPassword">MustResetPassword</label><select-bool id="ddlMustResetPassword" v-model="MustResetPassword" class="dropdown"></select-bool></div>
            <div class="twocols pt-10">
                <div class="form-field rCol">
                    <button v-on:click="doSearch()" class="button"> Search <i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>
    <div>`
});
