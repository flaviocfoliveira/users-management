let app = new Vue({
    el: "#app",
    data: {
        errors: [],
        usr: {},
        pass: ""
    },
    methods: {

        LoadUser: function (id) {

            fetch("/api/users/" + id, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
            })
                .then((response) => {
                    if (response.status == 200) {
                        return response.json();
                    } else {
                        return response.error();
                    }
                })
                .then((data) => {

                    this.usr = data;

                })
                .catch((error) => {
                    console.log('Error:', error);
                    SimpleNotification.error({
                        title: 'Error',
                        text: 'Ups, something went wrong.'
                    });
                });

        },

        UpdateName: function () {

            fetch("/api/users/" + this.usr.PublicID, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({FullName: this.usr.FullName}),
            })
                .then(response => {
                    if (response.status == 200) {

                        this.LoadUser(this.usr.PublicID);

                        SimpleNotification.success({
                            text: 'The name is updated'
                        });

                        return response.json();
                    }
                })
                .catch((error) => {
                    console.log('Error UpdateName:', error);
                    SimpleNotification.error({
                        title: 'Error',
                        text: 'Ups, something went wrong.'
                    });
                });

        },

        updateActive: function (e) {

            let v = (e === true) ? 1 : 0;

            fetch("/api/users/" + this.usr.PublicID + '/active/' + v, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
            })
                .then(response => {
                    if (response.status == 200) {

                        this.LoadUser(this.usr.PublicID);

                        let sts = v == 1 ? "activated" : "disabled";
                        SimpleNotification.success({
                            text: 'User was successfully ' + sts
                        });

                    }
                })
                .catch((error) => {
                    console.log('Error UpdateName:', error);
                    SimpleNotification.error({
                        title: 'Error',
                        text: 'Ups, something went wrong.'
                    });
                });
        },

        updateLocked: function (e) {

            let v = (e === true) ? 1 : 0;

            fetch("/api/users/" + this.usr.PublicID + '/locked/' + v, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
            })
                .then(response => {
                    if (response.status == 200) {

                        this.LoadUser(this.usr.PublicID);

                        let sts = v == 1 ? "locked" : "unlocked";
                        SimpleNotification.success({
                            text: 'User was successfully ' + sts
                        });
                    }
                })
                .catch((error) => {
                    console.log('Error UpdateName:', error);
                    SimpleNotification.error({
                        title: 'Error',
                        text: 'Ups, something went wrong.'
                    });
                });
        },

        updateConfirmed: function (e) {

            let v = (e === true) ? 1 : 0;

            fetch("/api/users/" + this.usr.PublicID + '/locked/' + v, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
            })
                .then(response => {
                    if (response.status == 200) {

                        this.LoadUser(this.usr.PublicID);

                        let sts = v == 1 ? "locked" : "unlocked";
                        SimpleNotification.success({
                            text: 'User was successfully ' + sts
                        });
                    }
                })
                .catch((error) => {
                    console.log('Error UpdateName:', error);
                    SimpleNotification.error({
                        title: 'Error',
                        text: 'Ups, something went wrong.'
                    });
                });
        },

        updateUMRP: function (e) {

            let v = (e === true) ? 1 : 0;

            fetch("/api/users/" + this.usr.PublicID + '/reset-password/' + v, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
            })
                .then(response => {
                    if (response.status == 200) {

                        this.LoadUser(this.usr.PublicID);

                        let sts = v == 1 ? "locked" : "unlocked";
                        SimpleNotification.success({
                            text: 'User must reset password was successfully changed to ' + e
                        });
                    }
                })
                .catch((error) => {
                    console.log('Error updateUMRP:', error);
                    SimpleNotification.error({
                        title: 'Error',
                        text: 'Ups, something went wrong.'
                    });
                });
        },

        updatePass: function (e) {

            this.errors = [];

            fetch("/api/users/" + this.usr.PublicID + '/new-password/' + encodeURI(this.pass), {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
            })
                .then(response => {
                    if (response.status == 200) {

                        this.LoadUser(this.usr.PublicID);

                        SimpleNotification.success({
                            text: 'New password was successfuly changed'
                        });
                    }

                    if (response.status == 400) {

                        response.json().then((o => {
                            this.errors = o.ErrorList;
                        }));

                    }
                })
                .catch((error) => {
                    console.log('Error updateUMRP:', error);
                    SimpleNotification.error({
                        title: 'Error',
                        text: 'Ups, something went wrong.'
                    });
                });
        },

    },
    delimiters: ['[[', ']]'],

});

