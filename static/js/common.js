Vue.component("select-bool", {
    props: {
        nullDesc: {default: " --- "},
        trueDesc: {default: "True"},
        falseDesc: {default: "False"},
        value: {default: ""},
    },
    template: `
    <select v-bind:value="value" v-on:input="$emit('input', $event.target.value)">
        <option value="">{{nullDesc}}</option>
        <option value="true">{{trueDesc}}</option>
        <option value="false">{{falseDesc}}</option>
    </select>`,
});

Vue.component("is-active", {
    props: {
        value: {default: ""},
        trueColor: {default: "00D31E"},
        falseColor: {default: "E21500"},
        disabledColor: {default: "D5D3D1"},
    },
    template: `<i class="fas fa-circle" v-bind:style="{ color: getcolor }"></i>`,
    computed: {
        getcolor: function () {
            switch (GetBool(this.value)) {
                case true:
                    return "#" + this.trueColor;
                case false:
                    return "#" + this.falseColor;
                default:
                    return "#" + this.disabledColor;
            }
        }
    }
});

Vue.component("bool-view", {
    props: {
        value: {type: Boolean, default: true},
        TrueClass: {type: String, default: "fas fa-toggle-on on"},
        TrueTitle: {type: String, default: "Yes"},
        FalseClass: {type: String, default: "fas fa-toggle-off off"},
        FalseTitle: {type: String, default: "No"},
        DisabledClass: {type: String, default: "fas fa-toggle-off dis"},
        DisabledTitle: {type: String, default: "disabled"},
        Nullable: {type: Boolean, default: true},
        Clickable: {type: Boolean, default: true},
    },
    data() {
        return {}
    },
    template: `<i v-bind:class="GetClass" v-on:click="toggle" v-bind:title="GetTitle"></i>`,
    computed: {
        GetClass: function () {
            let r = {};

            switch (GetBool(this.value)) {
                case true:
                    r[this.TrueClass] = true;
                    break
                case false:
                    r[this.FalseClass] = true;
                    break
                default:
                    r[this.DisabledClass] = true;
            }

            return r;
        },
        GetTitle: function () {
            switch (GetBool(this.value)) {
                case true:
                    return this.TrueTitle;
                case false:
                    return this.FalseTitle;
                default:
                    return this.DisabledTitle;
            }
        },
    },
    methods: {
        toggle: function () {
            if (GetBool(this.Clickable) === true) {

                if (GetBool(this.Nullable) === true) {
                    switch (GetBool(this.value)) {
                        case true:
                            this.value = false;
                            break;
                        case false:
                            this.value = null;
                            break;
                        default:
                            this.value = true;
                    }
                } else {
                    this.value = !this.value;
                }

                this.$emit('change', this.value);
            }
        }
    }
});

Vue.component("pager", {
    props: {
        Total: {default: 0},
        Page: {default: 1},
        PageSize: {default: 25},
        VisiblePages: {default: 9},
        PagerClass: {default: "pager"},
        PageClass: {default: "page"},
        PageActiveClass: {default: "page-active"},
    },
    template: `<div v-bind:class="PagerClass" v-bind:total="Total" v-bind:page-size="PageSize" v-bind:page="Page">
    <a v-for="p in Pages" v-on:click="PageChange(p.Num)" v-bind:class="PageClassObj(p)">{{p.Label}}</a></div>`,
    methods: {
        CreatePageItem: function (num, label) {
            let r = {Num: num, Label: label, Current: (num === this.Page)};
            if (r.Label === undefined || r.Label === null) {
                r.Label = r.Num;
            }
            return r;
        },
        PageClassObj: function (p) {
            let r = {};
            r[this.PageClass] = true;
            r[this.PageActiveClass] = p.Current;
            return r;
        },
        PageChange: function (page) {
            this.Page = page;
            let e = {
                Page: page,
                PageSize: this.PageSize,
            };
            console.log(e);
            this.$emit('page-change', e);
        }
    },
    computed: {
        TotalPages: function () {
            return Math.ceil(this.Total / this.PageSize);
        },
        Pages: function () {
            let result = [];

            if (this.TotalPages > 0) {
                if (this.TotalPages <= this.VisiblePages) {
                    for (let p = 0; p < this.TotalPages; p++) {
                        result.push(this.CreatePageItem(p + 1));
                    }
                } else {

                    let parts = ((this.VisiblePages - 1) / 2);
                    let start = this.Page - parts;
                    let end = this.Page + parts;

                    if (start < 1) {
                        let diff = 1 - start;
                        if (diff > 0) {
                            end += diff;
                            start -= diff;
                        }
                    }
                    if (end > this.TotalPages) {
                        let diff = end - this.TotalPages;
                        if (diff > 0) {
                            start -= diff;
                            end -= diff;
                        }
                    }

                    if (start < 1) start = 1;
                    if (end > this.TotalPages) end = this.TotalPages;

                    for (let p = start; p <= end; p++) {
                        result.push(this.CreatePageItem(p));
                    }

                    if (this.Page > 1) {
                        result.unshift(this.CreatePageItem(this.Page - 1, "<"));
                    }
                    if (start > 1) {
                        result.unshift(this.CreatePageItem(1, "<<"));
                    }
                    if (this.Page < this.TotalPages) {
                        result.push(this.CreatePageItem(this.Page + 1, ">"));
                    }
                    if (end < this.TotalPages) {
                        result.push(this.CreatePageItem(this.TotalPages, ">>"));
                    }

                }
            }

            this.$emit('update', {
                Page: this.Page,
                TotalPages: this.TotalPages,
                PageSize: this.PageSize,
            });

            return result;
        }
    }
});


Vue.component("errors-for", {
    props: {
        errors: {default: [], type: Array},
        field: {default: "", type: String},
        all: {default: false, type: Boolean},
        itemclass: {default: "error-item", type: String},
    },
    data: function () {
        return {
            items: []
        };
    },
    template: `
        <div v-if="!IsNullOrEmpty(items)" class="errors-for">
            <div v-for="e in items" class="error-item">{{e.Message}}</div>
        </div>`,
    watch: {
        errors: function (newVal, oldVal) {
            this.items = null;
            let f = this.field;

            if (!IsNullOrEmpty(this.field)) {
                $("#" + this.field).removeClass(this.itemclass);
            }

            if (newVal.length > 0) {
                if (this.all) {
                    this.items = newVal;
                } else {
                    this.items = [];
                    let output = [];

                    forEach(newVal, function (e, i) {
                        if (e.Field === f) output.push(e);
                    });

                    if (!IsNullOrEmpty(output)) {
                        this.items = output;
                        if (!IsNullOrEmpty(this.field)) {
                            $("#" + this.field).addClass(this.itemclass);
                        }
                    }
                }
            }
        }
    },
    computed: {}
});

function forEach(items, fn) {
    if (fn === undefined || fn == null) return;
    if (IsNullOrEmpty(items) || !Array.isArray(items)) return;

    for (let i = 0; i < items.length; i++) {
        fn(items[i], i);
    }
}

function IsNullOrEmpty(o) {
    return (o === undefined || o === null || o.length === 0);
}

function GetBool(v) {

    if (v === true || v === false) return v;

    if (v !== undefined && v != null && v.length > 0) {

        switch (v.trim().toLowerCase()) {
            case "true":
            case "1":
                return true;
            case "false":
            case "1":
                return false;
            default:
                throw 'value is not a boolean';
        }
    }

    return null;
}

function Coalesce() {

    for (let i = arguments.length; i != 0; i--) {
        if (arguments[i] != undefined && arguments[i] != null) {
            return arguments[i];
        }
    }

    return null;
}