let app = new Vue({
    el: "#app",
    data: {
        errors: [],
        fullName: "",
        email: "",
        password: "",
        passwordConf: ""
    },
    methods: {
        CreateUser: function () {

            this.errors = [];

            let obj = {
                fullName: this.fullName,
                email: this.email,
                password: this.password,
                passwordConf: this.passwordConf
            };

            fetch("/api/users", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(obj),
            })
                .then(response => {
                    if (response.status == 201) {

                        SimpleNotification.success({
                            text: 'New password was successfuly changed'
                        });

                        response.json().then((o => {
                            window.location = '/users/detail/' + o.Data;
                            console.log(o);
                        }));

                    }

                    if (response.status == 400) {

                        response.json().then((o => {
                            this.errors = o.ErrorList;
                        }));

                    }
                })
                .catch((error) => {
                    console.log('Error updateUMRP:', error);
                    SimpleNotification.error({
                        title: 'Error',
                        text: 'Ups, something went wrong.'
                    });
                });

        }
    },
    delimiters: ['[[', ']]']
});