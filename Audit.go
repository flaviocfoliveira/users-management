package main

import (
	"encoding/json"
	"os"

	"bitbucket.org/flaviocfoliveira/audit"
	"bitbucket.org/flaviocfoliveira/users"
	"github.com/gin-gonic/gin"
)

const (
	CURRENT_AUDITCONTEXT = "CurrentAuditContext"
)

func SerializeAuditData(obj interface{}) string {

	if obj != nil {

		if b, err := json.Marshal(obj); err != nil {
			panic(err)
		} else {
			return string(b)
		}

	}

	return ""
}

func AuditContextInjector() gin.HandlerFunc {
	return func(c *gin.Context) {

		hostname := ""
		if hn, err := os.Hostname(); err != nil {
			hostname = hn
		}

		userID := ""
		if u, exist := c.Get(CURRENT_USER); exist {
			userID = u.(*users.User).PublicID
		}

		ctx := AuditSvc.CreatAuditContext(c.ClientIP(), hostname, userID)
		if ctx != nil {
			c.Set(CURRENT_AUDITCONTEXT, ctx)
		}

		c.Next()
	}
}

func CurrentAuditContext(c *gin.Context) *audit.AuditContext {

	if c != nil {
		if ctx, exist := c.Get(CURRENT_AUDITCONTEXT); exist {
			return ctx.(*audit.AuditContext)
		}
	}

	return nil
}
