package main

import (
	"fmt"
	"html/template"
	"strings"
)

func errorsFor(e []ErrorItem, name string) template.HTML {

	if e != nil {
		r := make([]string, 0)
		for _, obj := range e {
			if obj.Field == name {
				r = append(r, fmt.Sprintf(`<li>%s</li>`, obj.Message))
			}
		}
		if len(r) > 0 {
			ul := fmt.Sprintf(`<ul class="error-item">%s</ul>`, strings.Join(r, ""))
			return template.HTML(ul)
		}
	}

	return ""
}
func errorClassFor(e []ErrorItem, name string) string {

	for _, obj := range e {
		if obj.Field == name {
			return "error-item"
		}
	}

	return ""
}
