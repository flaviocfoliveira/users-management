package main

import (
	"fmt"

	"bitbucket.org/flaviocfoliveira/sessions"
	"bitbucket.org/flaviocfoliveira/users"
)

type (
	Login struct {
		Email     string `form:"txtEmail"`
		Password  string `form:"txtPassword"`
		Csrf      string `form:"csrf"`
		ErrorList []ErrorItem
	}
	RegisterRequest struct {
		Name         string `form:"txtName"`
		Email        string `form:"txtEmail"`
		Password     string `form:"txtPassword"`
		PasswordConf string `form:"txtPassConf"`
		Csrf         string `form:"csrf"`
		ErrorList    []ErrorItem
	}
	ErrorItem struct {
		Field   string
		Message string
	}

	BaseResponse struct {
		User      *users.User
		Session   *sessions.Session
		ErrorList []ErrorItem
		Title     string
	}

	ApiResponse struct {
		Success   bool        `json:"Success"`
		ErrorList []ErrorItem `json:"ErrorList"`
		Data      interface{} `json:"Data"`
	}

	ChangePassword struct {
		BaseResponse

		OldPassword     string `form:"txtOldPassword"`
		NewPassword     string `form:"txtNewPassword"`
		NewPasswordConf string `form:"txtNewPasswordConf"`
	}

	ConfirmEmail struct {
		IsConfirmed bool
		ErrorList   []ErrorItem
		User        *users.User
		NewTokenUrl string
	}

	ResetPassword struct {
		Email     string
		ErrorList []ErrorItem
		SendEmail bool
	}

	ResetPasswordToken struct {
		Token           string
		NewPassword     string
		NewPasswordConf string
		ErrorList       []ErrorItem
		IsSuccess       bool
		Exists          bool
	}

	DashboardData struct {
		BaseResponse
	}
)

func (e ErrorItem) Error() string {

	if len(e.Field) > 0 {
		return fmt.Sprintf("{'Field':'%s', 'Message':'%s'}", e.Field, e.Message)
	}

	return fmt.Sprintf("{'Message':'%s'}", e.Message)
}
